#!/usr/bin/env python

import json
import urllib
from datetime import datetime

controllerIp = "192.168.1.2"

def query_switches():
	command = "http://%s:8080/wm/core/switch/all/flow/json" % controllerIp
	y = urllib.urlopen(command).read()
	return dict(json.loads(y))

def get_outbytes(switches):
	outbytes = {}

	for switch in switches:
		name = 'yoda%d' % int(switch[-5:-3])
		outbytes[name] = 0

		for flow in switches[switch]:
			if flow['match']['inputPort'] == -2:
				outbytes[name] += flow['byteCount']

	return outbytes

def get_outbytes_status():
	outbytes = get_outbytes(query_switches())
	return 'Status ' + str(datetime.now().time()) + ' ' + str(outbytes)

if __name__ == "__main__":
	print get_outbytes_status()
