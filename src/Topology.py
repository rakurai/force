import networkx as nx

class Topology:
	g = nx.Graph()

	def __init__(self, g):
		self.g = g

	def path(self, src, dst):
		return nx.shortest_path(self.g, src, dst)

	def add_node(self, name):
		self.g.add_node(name)
	#	print 'adding node %s' % name
		self.g.node[name]['maps_to'] = name

	def add_port(self, device, name):
		self.add_node(name)
		self.g.node[name]['role'] = 'port'
		self.add_link(device, name)

	def add_switch(self, name, ports=[], mac='none', filter_patterns=[]):
		self.add_node(name)
		self.g.node[name]['role'] = 'switch'
		self.g.node[name]['mac'] = mac
		self.g.node[name]['patterns'] = filter_patterns
		for port in ports:
			self.add_port(name, port)

	def add_host(self, name, ports=[], mac='none'):
		self.add_switch(name, ports, mac)
		self.g.node[name]['role'] = 'host'

	def add_link(self, src, dst, rev=True, gb=1):
	#	print 'adding link from %s to %s, %s way' % (src, dst, 'two' if rev == True else 'one')
		self.g.add_edge(src, dst)
		self.g[src][dst]['gb'] = gb
		if rev == True:
			self.add_link(dst, src, False, gb)

	def get_nodes(self, role=''):
		nodes = {}
		for node in self.g.nodes(data=True):
			if role is '' or node[1]['role'] == role:
				nodes[node[0]] = node[1]
		return nodes

	def get_hosts(self):
		return self.get_nodes('host')

	def get_switches(self):
		return self.get_nodes('switch')

	def get_ports(self):
		return self.get_nodes('port')

'''
	def get_port_num(switch, portname):
		parts = portname.split('_')
	#	if parts[0] != switch:
	#		print 'warning: port %s not part of switch %s' % (portname, switch)
		return int(parts[-1])

	def concat(g, h):
		g.add_nodes_from(h.nodes())
		g.add_edges_from(h.edges())

	def load(path):
		return nx.read_dot(path)

	def save(g, path):
		nx.write_dot(g, 'dot/' + path + '.dot')
'''
