import uuid
import ofutil
import os

restIP = '192.168.1.2:8080'
switches = {}

def match_convert(dict):
	match = {}
	for entry in dict:
		key = entry
		val = dict[entry]
		
		# translate keys
		if key == 'srcport':
			key = 'ingress-port'
		elif key == 'srcmac':
			key = 'src-mac'
		elif key == 'dstmac':
			key = 'dst-mac'
		
		# translate values
		match[key] = val
	return match

def add_flow(mac, match_dict, outport, priority=32768):
	switch_id = '00:00:'+mac
	if switch_id not in switches:
		switches[switch_id] = []

	match = match_convert(match_dict)

	# see if flow already exists
	for flow in switches[switch_id]:
		if len(flow['match']) != len(match):
			continue

		unique = False
		for key in match:
			if key not in flow['match'] or match[key] != flow['match'][key]:
				unique = True
				break

		if unique is False:
#			print match
			return

	flow = {}
	flow['id'] = str(uuid.uuid4())
	flow['match'] = match
	flow['priority'] = priority
	flow['active'] = 'true'
	flow['cookie'] = 0
	flow['outport'] = outport
	switches[switch_id].append(flow)

def push_flows():
	ofutil.clearflows_all()
	count = 0
	
	for switch in switches:
		for flow in switches[switch]:
			fields = '\"switch\": \"%s\"' % switch
			fields += ', \"name\":\"%s\"' % flow['id']
		
			for crit in flow['match']:
				fields += ', \"%s\":\"%s\"' % (crit, flow['match'][crit])

			fields += ', \"priority\":\"%d\"' % flow['priority']
			fields += ', \"active\":\"%s\"' % flow['active']
			fields += ', \"cookie\":\"%d\"' % flow['cookie']
			fields += ', \"actions\":\"output=%d\"' % flow['outport']
		
			command = "curl -s -d '{%s}' http://%s/wm/staticflowentrypusher/json" % (fields, restIP)
#			print command
			result = os.popen(command).read()
#			print result
			count += 1
#	print 'Pushed %d flows.' % count