#!/usr/bin/env python

# generate a simple 4 port switch and 4 connected hosts

import networkx as nx
import topo

g = nx.DiGraph()

switchname = 'switch'
topo.add_switch(g, switchname, 4)

for i in range(1, 5):
	hostname = 'yoda%d' % i
	topo.add_host(g, hostname, 1)

	hostport = '%s_1' % hostname
	switchport = '%s_%d' % (switchname, i)
	g.add_edge(hostport, switchport)
	g.add_edge(switchport, hostport)

topo.save(g, 'star-1s4-4h1')
