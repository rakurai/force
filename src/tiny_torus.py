#!/usr/bin/env python

import networkx as nx
import topo
import topology_torus as torus
import pathing
import yoda

torus.num_rows = 3
torus.num_cols = 4

g = nx.Graph()

def generate():
	ports = yoda.vswitch_ports()[1:]

	for node in yoda.nodes():
#		host = yoda.node_to_host(node)
		topo.add_node(g, node)

	for node in yoda.nodes():
		for dir in ports:
			neighbor = torus.neighbor(node, dir)
#			dst = yoda.node_to_host(neighbor)
			g.add_edge(node, neighbor)

# give full path from src to dst in context of *this* graph
def path(src, dst):
	path = [src]

#	cur_node = yoda.host_to_node(src)
#	dst_node = yoda.host_to_node(dst)
	cur_node = src
	dst_node = dst
	
	while cur_node != dst_node:
		direction = torus.path_step(cur_node, dst_node)
		next_node = torus.neighbor(cur_node, direction)
#		path.append(yoda.node_to_host(next_node))
		path.append(next_node)
		cur_node = next_node

	return path

def positions():
	pos = {}
	for node in yoda.nodes():
		y, x = torus.location(node)
#		pos[yoda.node_to_host(node)] = (x, -y)
		pos[node] = (x, -y)
	return pos
