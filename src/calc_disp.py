#!/usr/bin/env python

import topology_torus as torus

nrow = torus.num_rows
ncol = torus.num_cols

#def position(node):
#	rel_node = node -1
#	row = rel_node / nrow
#	col = rel_node % nrow
#	return row, col

def direction(srcnode, dstnode):
	srow, scol = torus.location(srcnode)
	drow, dcol = torus.location(dstnode)

	

	return 0

num_nodes = nrow*ncol
nodes = {}
for node in range(1, num_nodes+1):
	nodes[node] = {}
	for port in range(1, 5):
		nodes[node][port] = 0

for src in range(1, num_nodes+1):
	if src == 1 or src == 5 or src == 9:
		continue
	for dst in range(1, num_nodes+1):
		if dst != 1 or dst != 5 or dst != 9:
			continue
		cur = src
		while True:
			if cur is dst:
				break
			dir = torus.path_step(cur, dst)
			nodes[cur][dir] += 1
			cur = torus.neighbor(cur, dir)

dashed_line = "     ||     "*ncol

for row in range(0, nrow):
	line = ''
	for col in range(0, ncol):
		line += '    _||_    '
	print line
	line = '   '
	for col in range(0, ncol):
		line += '|    |  %2d  ' % nodes[node][2]
	print line
	line = '   '
	for col in range(0, ncol):
		line += '| %2d |======' % torus.node_at(row, col)
	print line
	line = '   '
	for col in range(0, ncol):
		line += '|____|  %2d  ' % nodes[torus.neighbor(node, 2)][4]
	print line
	print dashed_line
	line = ''
	for col in range(0, ncol):
		node = torus.node_at(row, col)
#		print node
		line += '   %2d||%2d   ' % (nodes[torus.neighbor(node, 1)][3], nodes[node][1])
	print line
	print dashed_line
				

#	print '%d - dir %d:%d steps, dir %d:%d steps, take %d' % (node, rdir, rdist, cdir, cdist, dir)
#	print '%d - take %d' % (node, dir)
for node in nodes.values():
	print node
