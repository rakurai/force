#!/usr/bin/env python

# generate an 8 port switch and 4 doubly connected hosts

import networkx as nx
import topo

g = nx.DiGraph()

#switchname = 'switch'
#switch = topo.gen_switch(switchname, 8)
#topo.concat(g, switch)

for i in range(1, 5):
	hostname = 'yoda%d' % i
	topo.add_host(g, hostname, 2)

	hostport = 'yoda%d_1' % (i)
	nextnum = i+1
	if nextnum is 5:
		nextnum = 1
	nextport = 'yoda%d_2' % nextnum
	g.add_edge(hostport, nextport)
	g.add_edge(nextport, hostport)

	# port mapping to parent topology, happens to be same names in this case
	for j in range(1, 3):
		hostname = 'yoda%d_%d' % (i, j)
		g.node[hostname]['mapping'] = hostname

topo.save(g, 'ring-4h2')
