import networkx as nx

def add_node(g, name):
	g.add_node(name)
#	print 'adding node %s' % name
	g.node[name]['maps_to'] = name
	g.node[name]['role'] = 'node'

def add_port(g, device, name):
	add_node(g, name)
	g.node[name]['role'] = 'port'
	add_link(g, device, name)

def add_switch(g, name, ports=[], mac='none', filter_patterns=[]):
	add_node(g, name)
	g.node[name]['role'] = 'switch'
	g.node[name]['mac'] = mac
	g.node[name]['patterns'] = filter_patterns
	for port in ports:
		add_port(g, name, port)

def add_host(g, name, ports=[], mac='none'):
	add_switch(g, name, ports, mac)
	g.node[name]['role'] = 'host'

def add_link(g, src, dst, rev=True, gb=1):
#	print 'adding link from %s to %s, %s way' % (src, dst, 'two' if rev == True else 'one')
	g.add_edge(src, dst)
	g[src][dst]['gb'] = gb
	if rev == True:
		add_link(g, dst, src, False, gb)

def get_port_num(switch, portname):
	parts = portname.split('_')
#	if parts[0] != switch:
#		print 'warning: port %s not part of switch %s' % (portname, switch)
	return int(parts[-1])

def concat(g, h):
	g.add_nodes_from(h.nodes())
	g.add_edges_from(h.edges())

def load(path):
	return nx.read_dot(path)

def save(g, path):
	nx.write_dot(g, 'dot/' + path + '.dot')

def get_nodes(g, role='node'):
	nodes = {}
	for node in g.nodes(data=True):
		if node[1]['role'] == role:
			nodes[node[0]] = node[1]
	return nodes

def get_hosts(g):
	return get_nodes(g, 'host')

def get_switches(g):
	return get_nodes(g, 'switch')

def get_ports(g):
	return get_nodes(g, 'port')
