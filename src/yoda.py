import subprocess
import shlex

# node: a number, 1..num_nodes
# host: a string, 'yoda1'...
# mac: a string, '00:00:00:00:05:01' (for yoda5)
# vswitch: a string, '00:00:mac'

num_nodes = 12
node_map = range(1, num_nodes+1) # can be remapped for different arrangements

#pronto_mac = "00:00:12:34:56:ab:cd:ef"
pronto_name = 'pronto'
pronto_mac = "00:00:00:00:00:01"
pronto_num_ports = 48

def print_torus():
	print hosts()
	for row in range(0, 3):
		line = '* '
		for col in range(1, 5):
			line = '%s%8s' % (line, node_to_host(row*4+col))
		print line

def nodes():
	return range(1, num_nodes+1)

def hosts():
	return [node_to_host(node) for node in nodes()]

def vswitch_ports():
	return [65534, 1, 2, 3, 4]

def pronto_ports():
	return range(1, pronto_num_ports+1)

def map_node(node):
	return node_map[node-1]

def unmap_node(node):
	return node_map.index(node)+1

# is_...
def is_node(node):
	return True if int(node) in nodes() else False

def is_host(host):
	return True if host[:4] == 'yoda' and is_node(host_to_node(host)) else False

def is_switch(switch):
	return True if is_host(switch_to_host(switch)) and switch[-2:] == '_s' else False

# node_to_...
def node_to_host(node):
	return 'yoda%d' % map_node(node)

def node_to_portname(node, port):
	return host_to_portname(node_to_host(node), port)

def node_to_mac(node):
	return "aa:aa:aa:aa:%02d:01" % map_node(node)

def node_to_switch(node):
	return host_to_switch(node_to_host(node))

def node_to_vswitch_id(node):
	return mac_to_switch_id(node_to_mac(node))

def node_to_ip(node):
	return host_to_ip(node_to_host(node))

def node_to_pronto_port(node, port):
	return (map_node(node) - 1) * 4 + port

# host_to_...
def host_to_node(host):
	return unmap_node(int(host[4:]))

def host_to_portname(host, port):
	return '%s_%d' % (host, port)

def host_to_mac(host):
	return node_to_mac(host_to_node(host))

def host_to_switch(host):
	return host + '_s'

def host_to_switch_id(host):
	return node_to_switch_id(host_to_node(host))

def host_to_ip(host):
	cmd = 'dig ' + host + ' +short'
	proc = subprocess.Popen(shlex.split(cmd), stdout = subprocess.PIPE)
	out, err = proc.communicate()
	return str.strip(out)
	return "temp" + host

def host_to_pronto_port(host, port):
	return node_to_pronto_port(host_to_node(host), port)

# switch_to_...
def switch_to_node(switch):
	return host_to_node(switch_to_host(switch))
	
def switch_to_host(switch):
	return switch.split('_')[0]

def switch_to_portname(switch, port):
	return '%s_%d' % (switch, port)

# mac_to_...
def mac_to_node(mac):
	return unmap_node(int(mac[12:14]))

def mac_to_host(mac):
	return node_to_host(mac_to_node(mac))

def mac_to_switch(mac):
	return host_to_switch(mac_to_host(mac))

def mac_to_switch_id(mac):
	return '00:00:' + mac

def mac_to_ip(mac):
	return 'not yet implemented'

# portname_to_...
def portname_split(portname):
	return portname.split('_')

def portname_to_node(portname):
	return host_to_node(portname_to_host(portname))

def portname_to_host(portname):
	return portname_split(portname)[0]

def portname_to_port(portname):
	return int(portname_split(portname)[-1])

def portname_to_switch(portname):
	spl = portname_split(portname)
	assert(len(spl) is 3)
	return spl[1]
	
def portname_to_mac(portname):
	return host_to_mac(portname_to_host(portname))
