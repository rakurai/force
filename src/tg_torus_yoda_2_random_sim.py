#!/usr/bin/env python

import networkx as nx
import topo
import topology_torus as torus
import pathing
import yoda
import random
import pickle

g = nx.DiGraph()

""""
f = open('yoda_hosts.pkl', 'r')
hosts = pickle.load(f)
f.close()

nodes = []
for host in hosts:
	nodes.append(yoda.host_to_node(host))
yoda.node_map = nodes
"""

#random.shuffle(yoda.node_map)

def generate():
	g.clear()

	hosts = yoda.hosts()
#	print yoda.node_map

	for host in hosts:
		switch = yoda.host_to_switch(host)
		topo.add_switch(g, yoda.host_to_switch(host))
		topo.add_host(g, host)
		topo.add_link(g, host, switch, gb=48)

	for node in yoda.nodes():
		for dir in yoda.vswitch_ports()[1:]:
			neighbor = torus.neighbor(node, dir)
			src = yoda.node_to_switch(node)
			dst = yoda.node_to_switch(neighbor)
			topo.add_link(g, src, dst, False, gb=1)

# give full path from src to dst in context of *this* graph
def path(src, dst):
	path = [src]
	src_switch = src
	dst_switch = dst

	if not yoda.is_switch(src_switch):
		src_switch = yoda.host_to_switch(src)
		path = [src, src_switch]

	if not yoda.is_switch(dst):
		dst_switch = yoda.host_to_switch(dst)

	cur_node = yoda.switch_to_node(src_switch)
	dst_node = yoda.switch_to_node(dst_switch)

	while cur_node != dst_node:
		direction = torus.path_step(cur_node, dst_node)
		next_node = torus.neighbor(cur_node, direction)
		path.append(yoda.node_to_switch(next_node))
		cur_node = next_node

	if not yoda.is_switch(dst):
		path.append(dst)

	return path

def draw():
	pos = {}
	for node in yoda.nodes():
		y, x = torus.location(node)
		pos[yoda.node_to_switch(node)] = (x, -y)
		pos[yoda.node_to_host(node)] = (x + .2, -y + .2)
	nx.draw(g, pos)
