#!/usr/bin/env python

import networkx as nx
import topo
import topology_torus as torus
import pathing
import yoda

g = nx.DiGraph()

def generate():
	hosts = yoda.hosts()

	for host in hosts:
		switch = yoda.host_to_switch(host)
		topo.add_host(g, host)
		topo.add_switch(g, yoda.host_to_switch(host))
		topo.add_link(g, host, switch)

	for src in hosts:
		src_switch = yoda.host_to_switch(src)

		for dst in hosts:
			if src is dst:
				continue

			dst_switch = yoda.host_to_switch(dst)
			topo.add_link(g, src_switch, dst_switch, False)

# give full path from src to dst in context of *this* graph
def path(src, dst):
	return nx.shortest_path(g, src, dst)

def draw():
	circles = []
	circles.append(sorted(topo.get_switches(g)))
	circles.append(sorted(topo.get_hosts(g)))
	pos = nx.shell_layout(g, circles)
	nx.draw(g, pos)

