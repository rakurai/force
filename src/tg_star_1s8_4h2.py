#!/usr/bin/env python

# generate an 8 port switch and 4 doubly connected hosts

import networkx as nx
import topo

g = nx.DiGraph()

switchname = 'switch'
topo.add_switch(g, switchname, 8)

for i in range(1, 5):
	hostname = 'yoda%d' % i
	topo.add_host(g, hostname, 2)

	for p in range(1, 3):
		hostport = '%s_%d' % (hostname, p)
		switchport = '%s_%d' % (switchname, ((i-1)*2)+p)
		g.add_edge(hostport, switchport)
		g.add_edge(switchport, hostport)

topo.save(g, 'star-1s8-4h2')
