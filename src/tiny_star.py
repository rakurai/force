#!/usr/bin/env python

# generate an 8 port switch and 4 doubly connected hosts

import networkx as nx
import topo
import yoda

g = nx.MultiGraph()
yoda.num_nodes = 12
yoda.pronto_num_ports = 48

def generate():
	pronto = "pronto"
	g.add_node(pronto)

	for node in yoda.nodes():
#		host = yoda.node_to_host(node)
		host = node

		topo.add_node(g, host)
		for i in range(5):
			g.add_edge(host, pronto, i)
#		g.add_edge(host, pronto)
#		g.add_edge(pronto, host)

# give full path from src to dst in context of *this* graph
def path(src, dst):
	return nx.shortest_path(g, src, dst)

def positions():
	pos = {}
	pos['pronto'] = (5.5, 1)
	for node in yoda.nodes():
		pos[node] = (node-1, 0)
#		y, x = torus.location(node)
#		pos[yoda.node_to_host(node)] = (x, -y)
#		pos[node] = (x, -y)
	return pos

#	return nx.graphviz_layout(g, prog='dot')
