#!/usr/bin/env python

# generate an 8 port switch and 4 doubly connected hosts

import networkx as nx
import topo
import yoda

g = nx.DiGraph()

def generate():
	# big switch
	pronto = yoda.host_to_switch(yoda.pronto_name)
	ports = [yoda.switch_to_portname(pronto, p) for p in yoda.pronto_ports()]
	topo.add_switch(g, pronto, ports)
	g.node[pronto]['mac'] = yoda.pronto_mac
	g.node[pronto]['patterns'] = [['srcport']]

	for node in yoda.nodes():
		host = yoda.node_to_host(node)
		switch = yoda.host_to_switch(host)
		ports = [yoda.switch_to_portname(switch, p) for p in yoda.vswitch_ports()]
		topo.add_switch(g, switch, ports)
		g.node[switch]['mac'] = yoda.node_to_mac(node)
		g.node[switch]['patterns'] = [['srcport', 'dstmac']]

		for port in ports[1:]:
			psport = yoda.switch_to_portname(pronto, yoda.node_to_pronto_port(node, yoda.portname_to_port(port)))
			topo.add_link(g, port, psport)

		hostport = yoda.host_to_portname(host, 1)
		topo.add_host(g, host, [hostport])
		g.node[host]['mac'] = yoda.node_to_mac(node)
		topo.add_link(g, hostport, ports[0])

# give full path from src to dst in context of *this* graph
def path(src, dst):
	return nx.shortest_path(g, src, dst)

def draw():
	pos = nx.graphviz_layout(g, prog='dot')
	nx.draw(g, pos)
