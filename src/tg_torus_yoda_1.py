#!/usr/bin/env python

import networkx as nx
import topo
import topology_torus as torus
import pathing
import yoda

torus.num_rows = 3
torus.num_cols = 4

g = nx.DiGraph()

def generate():
	hosts = yoda.hosts()
	ports = yoda.vswitch_ports()[1:]

	for host in hosts:
		switch = yoda.host_to_switch(host)
		topo.add_switch(g, switch, [yoda.switch_to_portname(switch, p) for p in ports])

		# host and switch ports defined in upper layer, collapse here
		topo.add_host(g, host)
		topo.add_link(g, host, switch)

	for node in yoda.nodes():
		for dir in ports:
			neighbor = torus.neighbor(node, dir)
			src = yoda.switch_to_portname(yoda.node_to_switch(node), dir)
			dst = yoda.switch_to_portname(yoda.node_to_switch(neighbor), torus.rev_dir(dir))
			topo.add_link(g, src, dst, False)

# give full path from src to dst in context of *this* graph
def path(src, dst):
	return nx.shortest_path(g, src, dst)

def draw():
	pos = {}
	for node in yoda.nodes():
		y, x = torus.location(node)
		pos[yoda.node_to_switch(node)] = (x, -y)
		pos[yoda.node_to_host(node)] = (x + .2, -y + .2)
		for dir in yoda.vswitch_ports[1:]:
			xmod, ymod = torus.dirmod(dir)
			pos[yoda.switch_to_portname(yoda.node_to_switch(node), dir)] = (x + xmod * .2, -y + ymod * .2)
	nx.draw(g, pos)
