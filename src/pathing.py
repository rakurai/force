import topo
import networkx as nx
import sdn

# come up with a set of flows for shortest path from each host to each host
# this may not be very smart, as won't distribute evenly over multiple paths
def all_to_all(g):
	paths = []
	hosts = topo.get_hosts(g)

	for src in hosts:
		src_data = hosts[src]

		for dst in hosts:
			if src == dst:
				continue

			dst_data = hosts[dst]
			path = nx.shortest_path(g, src, dst)

			# fully expand the path
			paths.append(expand_path(g, path))
	return paths

def expand_path(g, path):
	fullpath = [path[0]]
	for x in range(0, len(path)-1):
		fullpath += g[path[x]][path[x+1]]['path'][1:]
	return fullpath

def paths_to_flows(g_root, paths):
	switches = topo.get_switches(g_root)
	hosts = topo.get_hosts(g_root)

	for path in paths:
		src = path[0]
		dst = path[-1]

		for x in range(1, len(path)-1):
			if path[x] not in switches:
				continue

			switch = path[x]
			match = {
				'srcport':str(topo.get_port_num(switch, path[x-1])),
				'srcmac':hosts[src]['mac'],
				'dstmac':hosts[dst]['mac'] # does this map to root host name?
			}

			pattern = switches[switch]['patterns'][0]
			rlist = []
			for key in match:
				if key not in pattern:
					rlist.append(key)
			for key in rlist:
				del match[key]

			sdn.add_flow(switches[switch]['mac'], match, topo.get_port_num(switch, path[x+1]))
