
# grid pattern
#      c0 c1 c2 c3 
#     ------------
# r0 |  1  2  3  4
# r1 |  5  6  7  8
# r2 |  9 10 11 12

num_cols = 4
num_rows = 3

def num_nodes():
	return num_cols*num_rows

def node_at(row, col):
	return (num_cols * row + col + 1)

def rev_dir(dir):
	return ((dir+1)%4)+1

# return row and column, zero indexed
def location(node):
	znode = node-1
	col = znode % num_cols
	row = znode / num_cols
	return row, col

def corrected_val(val, max):
	while val < 0:
		val += max
	return val % max

def step_to(line1, line2, n):
	for x in range(0, n):
		up = down = False

		if corrected_val(line1 + x, n) is line2:
			down = True
		if corrected_val(line1 - x, n) is line2:
			up = True

		if up is True and down is True:
			return 0, x
		if up is True:
			return 1, x
		if down is True:
			return 3, x
	return -1

def step_to_row(srow, drow):
	dir, dist = step_to(srow, drow, num_rows)
	return dir, dist

def step_to_col(scol, dcol):
	dir, dist = step_to(scol, dcol, num_cols)
	if dir is not 0:
		dir = corrected_val(dir+2, num_cols)
		dir = dir +1
	return dir, dist

def path_step(s, d):
#	if s is d:
#		return 0

	srow, scol = location(s)
	drow, dcol = location(d)
	tiebreaker = ((drow + dcol) % 2)*2+1 # 1 or 3, depending on even/odd of drow+dcol
	vdir, vdist = step_to_row(srow, drow)
	hdir, hdist = step_to_col(scol, dcol)
#	print 'src %d (%d,%d), dst %d (%d,%d), vdir %d, vdist %d, hdir %d, hdist %d, tb %d' % \
#		(s, srow, scol, d, drow, dcol, vdir, vdist, hdir, hdist, tiebreaker)
	if vdir is 0:
		vdir = tiebreaker

	if hdir is 0:
		hdir = tiebreaker+1

	if vdist > hdist: # preferring vertical movement
		dir = vdir
		dist = vdist
	else:
		dir = hdir
		dist = hdist

	if dist is 0:
		return 0
	return dir

def corrected_row(row):
	return corrected_val(row, num_rows)

def corrected_col(col):
	return corrected_val(col, num_cols)

def dirmod(direction):
	x = 1 if direction is 2 else -1 if direction is 4 else 0
	y = 1 if direction is 3 else -1 if direction is 1 else 0
	return x, y

def neighbor(node, direction):
	row, col = location(node)
	xmod, ymod = dirmod(direction)
	return node_at(corrected_row(row + ymod), corrected_col(col + xmod))

def placement_score(M, placed):
	sum = 0
	for i_node in range(num_nodes()):
		node = placed[i_node]
		for direction in range(1, 3):
			i_nbr = neighbor(i_node+1, direction)-1
			nbr = placed[i_nbr]
			sum += M[node-1][nbr-1]
	return sum

# generate a placement scheme starting at loc, given strengths and nodes already placed, return sum of relationship strengths
# strengths = [12] where val is sum of row n in M, placed = finished [12], M is 12x12 matrix of internode strengths
# returns: best sum of strengths for possible recursive placements
# evaluates: M, loc
# updates: strengths, placed
attempt = [0 for x in range(0, 15)]
def recursive_placement(M, loc, placed, depth=0):
#	print 'depth =', depth
#	print strengths
#	print placed
	cur_node = placed[loc-1]
#	print attempt, 'placing in loc', loc

	# choose the node that will be placed near loc (finding most important node to the one placed in loc)
	to_place = None
	best_link = -1
	for i in range(1, num_nodes()+1):
		if i in placed:
			continue
		if M[cur_node-1][i-1] > best_link:
			best_link = M[cur_node-1][i-1]
			to_place = i
	if to_place == None:
		return 0
#	print 'choosing node index ', node_index, 'which is str ', strengths[node_index]

	# base case: all important nodes are placed, scatter remaining
#	if M[cur_node-1][to_place-1] == 0 or depth > 3:
#		print 'found strength 0, placing the rest'
#		for i in range(1, num_nodes()+1):
#			if i not in placed:
				# put in the first 0 slot
#				placed[placed.index(0)] = i
#		return 0

#	strengths[node_index] = 0		# zero the strength for recursing
#	placed[loc-1] = node_index+1	# place the node in the slot
#	print 'placed', node_index+1
	# find the best direction to go for the next placement
	best_sum = 0			# best sum so far
	best_strengths = None	# will be a copy of strengths for modifying by recursion
	best_placed = None		# will be a copy of placed for modifying by recursion
	best_loc = None			# best location in torus to fill in next, by highest sum
	
	for direction in range(1, 5):
		test_loc = neighbor(loc, direction) # where to go next
		if placed[test_loc-1] != 0:
			continue
		placed_copy = [x for x in placed]
		placed_copy[test_loc-1] = to_place

		attempt[depth] = direction
		sum = recursive_placement(M, test_loc, placed_copy, depth+1)
		
		if sum > best_sum or best_sum == 0:
			best_placed = placed_copy
			best_sum = sum
			best_loc = test_loc

	if best_loc == None:
		return 0
	print 'placing,', to_place, 'in loc', best_loc, 'had the best sum of', best_sum, 'at depth', depth

	# copy best results into the parameter lists for return to calling function
	for i in range(0, num_nodes()):
		placed[i] = best_placed[i]

	return M[cur_node-1][to_place-1] + best_sum

def recursive_placement2(M, loc, placed, waiting=0, depth=0):
#	print 'depth =', depth
#	print strengths
#	print placed
	cur_node = placed[loc-1]
#	print attempt, 'placing in loc', loc

	# choose the node that will be placed near loc (finding most important node to the one placed in loc)
	to_place = None
	best_link = -1
	next_best = -1
	for i in range(1, num_nodes()+1):
		if i in placed:
			continue
		if M[cur_node-1][i-1] > best_link:
			next_best = best_link
			best_link = M[cur_node-1][i-1]
			to_place = i
	if to_place == None:
		return 0

	# if there was a more important node to place above, stop here
	if M[cur_node-1][to_place-1] <= waiting:
		return 0
#	print 'choosing node index ', node_index, 'which is str ', strengths[node_index]

	# base case: all important nodes are placed, scatter remaining
#	if M[cur_node-1][to_place-1] == 0 or depth > 3:
#		print 'found strength 0, placing the rest'
#		for i in range(1, num_nodes()+1):
#			if i not in placed:
				# put in the first 0 slot
#				placed[placed.index(0)] = i
#		return 0

#	strengths[node_index] = 0		# zero the strength for recursing
#	placed[loc-1] = node_index+1	# place the node in the slot
#	print 'placed', node_index+1
	# find the best direction to go for the next placement
	best_sum = 0			# best sum so far
	best_strengths = None	# will be a copy of strengths for modifying by recursion
	best_placed = None		# will be a copy of placed for modifying by recursion
	best_loc = None			# best location in torus to fill in next, by highest sum
	
	for direction in range(1, 5):
		test_loc = neighbor(loc, direction) # where to go next
		if placed[test_loc-1] != 0:
			continue
		placed_copy = [x for x in placed]
		placed_copy[test_loc-1] = to_place

		attempt[depth] = direction
		sum = recursive_placement2(M, test_loc, placed_copy, next_best, depth+1)
		
		if sum > best_sum or best_sum == 0:
			best_placed = placed_copy
			best_sum = sum
			best_loc = test_loc

	if best_loc == None:
		return 0
	print 'placing,', to_place, 'in loc', best_loc, 'had the best sum of', best_sum, 'at depth', depth

	# copy best results into the parameter lists for return to calling function
	for i in range(0, num_nodes()):
		placed[i] = best_placed[i]

	return M[cur_node-1][to_place-1] + best_sum












