#! /usr/bin/env python

import os
import yoda

controllerRestIp = "192.168.1.2:8080"

# clear the flows
def clearflows(switch):
	command = "curl -s http://%s/wm/staticflowentrypusher/clear/%s/json" % (controllerRestIp, switch)
	result = os.popen(command).read()

def clearflows_vs():
	for i in range(1, 13):
		clearflows(yoda.hostname_to_vswitch_id("yoda%d" % i))
		
def clearflows_ps():
	clearflows(yoda.of_switch_id)

def clearflows_all():
	command = "curl -s http://%s/wm/staticflowentrypusher/clear/all/json" % controllerRestIp
	result = os.popen(command).read()

