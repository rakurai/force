import floodlight as controller

def add_flow(mac, match_dict, outport):
	return controller.add_flow(mac, match_dict, outport)

def push_flows():
	return controller.push_flows()
