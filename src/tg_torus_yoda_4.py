#!/usr/bin/env python

import networkx as nx
import topo
import topology_torus as torus
import pathing
import yoda

g = nx.DiGraph()

def generate():
	hosts = yoda.hosts()

	for host in hosts:
		topo.add_host(g, host)

	for src in hosts:
		for dst in hosts:
			if src is dst:
				continue

			topo.add_link(g, src, dst, False)

# give full path from src to dst in context of *this* graph
def path(src, dst):
	return [src, dst]

def draw():
	pos = nx.shell_layout(g, [sorted(g.nodes())])
	nx.draw(g, pos)
