import Topology as topology
import yoda

class YodaPhys:
	topo = topology.Topology()
	graph = nx.Graph

	def __init__(self):
		# big switch
		pronto = yoda.host_to_switch(yoda.pronto_name)
		ports = [yoda.switch_to_portname(pronto, p) for p in yoda.pronto_ports]
		self.topo.add_switch(pronto, ports)
		g.node[pronto]['mac'] = yoda.pronto_mac
		g.node[pronto]['patterns'] = [['srcport']]

		for node in yoda.nodes():
			host = yoda.node_to_host(node)
			switch = yoda.host_to_switch(host)
			ports = [yoda.switch_to_portname(switch, p) for p in yoda.vswitch_ports]
			self.topo.add_switch(switch, ports)
			g.node[switch]['mac'] = yoda.node_to_mac(node)
			g.node[switch]['patterns'] = [['dstmac']]

			for port in ports[1:]:
				psport = yoda.switch_to_portname(pronto, yoda.node_to_pronto_port(node, yoda.portname_to_port(port)))
				self.topo.add_link(port, psport)

			hostport = yoda.host_to_portname(host, 1)
			self.topo.add_host(host, [hostport])
			g.node[host]['mac'] = yoda.node_to_mac(node)
			self.topo.add_link(hostport, ports[0])
	
