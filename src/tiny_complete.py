#!/usr/bin/env python

import networkx as nx
import topo
import topology_torus as torus
import pathing
import yoda

g = nx.Graph()

def generate():
#	hosts = yoda.hosts()
	hosts = yoda.nodes()

	for host in hosts:
		topo.add_node(g, host)

	for src in hosts:
		for dst in hosts:
			if src is dst:
				continue
			g.add_edge(src, dst)

# give full path from src to dst in context of *this* graph
def path(src, dst):
	return [src, dst]

def positions():
	return nx.shell_layout(g, [sorted(topo.get_nodes(g))])
