#! /usr/bin/env python

import sys
import os
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/src')
#print sys.path
import sdn
import pathing

def load_setfile(setfile_name):
	file = open(setfile_name, 'r')
	layer_names = []

	for line in file:
		layer_name = line.strip()

		if len(layer_name) <= 0:
			continue
		if layer_name[0] == '#':
			continue

		layer_names.append(layer_name)

	file.close()
	return layer_names

def load_layers(layer_names):
	layers = []

	for layer_name in layer_names:
#		print 'Loading layer %s.' % layer_name
		layer = {}
		layer['name'] = layer_name
		layer['module'] = __import__(layer_name)
		layer['module'].generate()
		layer['graph'] = layer['module'].g
		layers.append(layer)

#		plt.clf()
#		layer['module'].draw()
#		plt.savefig('plot/' + module_name + '.png')
	return layers

def compound_edges(layers):
	prev_layer = layers[0]

	for edge in prev_layer['graph'].edges(data=True):
		edge[2]['path'] = [edge[0], edge[1]]

	for layer in layers[1:]:
		g = layer['graph']

#		for node in g.nodes(data=True):
#			if node[1]['maps_to'] not in prev_layer['graph'].nodes():
#				sys.exit('%s does not map to a lower level node' % node[1]['maps_to'])

		for edge in g.edges_iter():
			src = edge[0]
			dst = edge[1]
			psrc = g.node[src]['maps_to']
			pdst = g.node[dst]['maps_to']
			path = prev_layer['module'].path(psrc, pdst)
			fullpath = [psrc]

			for x in range(0, len(path)-1):
				p1 = path[x]
				p2 = path[x+1]
				fullpath += prev_layer['graph'][p1][p2]['path'][1:]

			g[src][dst]['path'] = fullpath

		prev_layer = layer

def load_topology(setfile_name):
	layer_names = load_setfile(setfile_name)
	layers = load_layers(layer_names)

	if len(layers) > 0:
		compound_edges(layers)

	return layers

if __name__ == "__main__":
	usage = "Usage: %s set_file" % sys.argv[0]

	if len(sys.argv) != 2:
		sys.exit(usage)

	layers = load_topology(sys.argv[1])

	if layers == []:
		sys.exit("Layers file has no readable topology modules.")

	paths = pathing.all_to_all(layers[-1]['graph'])
	pathing.paths_to_flows(layers[0]['graph'], paths)
	sdn.push_flows()
