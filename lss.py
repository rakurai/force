#! /usr/bin/env python

import sys
import os
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/src')
import force
import pickle
import networkx as nx
import matplotlib.pyplot as plt
import yoda
import topology_torus as torus
import pprint

# input a layer and a set of relationship tuples
def calculate_lss(layer, pairs):
	g = layer['graph']

	# build the path utilization matrix
	paths = {}
	for [src, dst] in pairs:
		if src not in paths:
			paths[src] = {}

		if dst not in paths[src]:
			paths[src][dst] = {}

			# save the path
			path = paths[src][dst]['path'] = layer['module'].path(src, dst)
#			print path

			# find the bottleneck in this path
			mn = min([g[path[x]][path[x+1]]['gb'] for x in range(0, len(path)-1)])
			paths[src][dst]['bottleneck'] = mn
			paths[src][dst]['uses'] = 0

		paths[src][dst]['uses'] += 1

#	print pprint.pprint(paths)
	# build the edge utilization matrix
	edge_weights = {}
	for src in paths:
		for dst in paths[src]:
			path = paths[src][dst]['path']
			usage = paths[src][dst]['uses'] * paths[src][dst]['bottleneck']

			# find the bottleneck in this path
			for x in range(0, len(path)-1):
				psrc = path[x]
				pdst = path[x+1]
				if psrc not in edge_weights:
					edge_weights[psrc] = {}
				if pdst not in edge_weights[psrc]:
					edge_weights[psrc][pdst] = 0.0
				
				edge_weights[psrc][pdst] += usage
#	print pprint.pprint(edge_weights)
	''''
	# construct for drawing
	pos = {}
	for node in yoda.nodes():
		y, x = torus.location(node)
		yloc = -y
		xloc = x
		if x == 0 or x == 3:
			yloc = yloc - 0.2
		if y == 0 or y == 2:
			xloc = xloc - 0.2
		pos[yoda.node_to_switch(node)] = (xloc, yloc)
		pos[yoda.node_to_host(node)] = (xloc + .2, yloc + .2)
	nx.draw_networkx_nodes(g, pos, ax=None)

	ew = {}
	for src in edge_weights:
		for dst in edge_weights[src]:
			wt = edge_weights[src][dst]
			if wt not in ew:
				ew[wt] = []
			ew[wt].append((src, dst))

	weights = sorted(ew.keys())
	for wt in weights:
		nx.draw_networkx_edges(g, pos, ax=None, edgelist=ew[wt], width=wt/weights[0], arrows=False)

	labels = {}
	for src in paths:
		if src not in labels:
			labels[src] = 'M'
		for dst in paths[src]:
			if dst not in labels:
				labels[dst] = 'R'

	nx.draw_networkx_labels(g, pos, ax=None, labels=labels)
	plt.savefig('plot.png')
	'''
	# find the average path weight
	total_weight = 0
	total_paths = 0
	for src in paths:
		for dst in paths[src]:
			path = paths[src][dst]['path']
			# get the lowest throughput link along the path
			mn = min(
#				[g[path[x]][path[x+1]]['gb']**2 / edge_weights[path[x]][path[x+1]]
				[g[path[x]][path[x+1]]['gb']*paths[src][dst]['bottleneck'] / edge_weights[path[x]][path[x+1]]
				for x in range(0, len(path)-1)]
			)
#			print 'path is using %f of its capacity' % mn
			# turn into a fraction of actual throughput / best throughput
			mn = min(mn/paths[src][dst]['bottleneck'], 1)

			uses = paths[src][dst]['uses']
			total_weight += mn * uses
			total_paths += uses

	score = total_weight/total_paths
#	print 'Weight %d, Paths %d, Score %f' % (total_weight, total_paths, score)
	return score
	
yoda_topology = None
def yoda_lss(arrangement, transfers):
	global yoda_topology
	if yoda_topology is None:
#		print 'reloading topology'
		yoda_topology = force.load_topology(os.path.dirname(os.path.realpath(__file__)) + "/sets/yoda_t.set")
#	print 'using lens ', arrangement
	# this could be a matrix operation?
	pairs = []
	for [src, dst] in transfers:
#		print 'translating %s and %s' % (src, dst)
		src = yoda.node_to_host(arrangement.index(yoda.host_to_node(src))+1)
		dst = yoda.node_to_host(arrangement.index(yoda.host_to_node(dst))+1)
#		print 'to          %s and %s' % (src, dst)
		pairs.append([src, dst])

#	print transfers
#	print pairs
	return calculate_lss(yoda_topology[-1], pairs)

if __name__ == "__main__":
	usage = "Usage: %s set_file.set transfers_file.pkl" % sys.argv[0]

	if len(sys.argv) < 3:
		sys.exit(usage)

	layers = force.load_topology(sys.argv[1])

	if layers == []:
		sys.exit("Layers file has no readable topology modules.")

	f = open(sys.argv[2], 'r')
	transfers = pickle.load(f)
	f.close()

	print calculate_lss(layers[-1], transfers)
