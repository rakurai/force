#! /usr/bin/env python

import os
import sys
import yoda
import ofutil
import topology_torus as torus

# clear the flows
ofutil.clearflows_vs()

for snode in range(1, 13):
	for sport in range(1, 5):
		dnode = torus.neighbor(snode, sport)
		dport = ((sport+1) % 4) + 1

		flows = []
		flow = {}

		# return traffic from destination node
		flow['switch'] = yoda.hostname_to_vswitch_id("yoda%d" % dnode)
		flow['match'] = {}
		flow['name'] = "%d:%d_out" % (dnode, dport)
		flow['match']['ingress-port'] = 65534
		flow['outport'] = dport
		flows.append(flow.copy())
		flow['match'] = {}
		flow['name'] = "%d:%d_in" % (dnode, dport)
		flow['match']['dst-mac'] = yoda.node_to_vswitch_mac(dnode)
		flow['outport'] = 0
		flows.append(flow.copy())

		# link on source node
		flow['switch'] = yoda.hostname_to_vswitch_id("yoda%d" % snode)
		flow['match'] = {}
		flow['name'] = "%d:%d_out" % (snode, sport)
		flow['match']['ingress-port'] = 65534
		flow['outport'] = sport
		flows.append(flow.copy())
		flow['match'] = {}
		flow['name'] = "%d:%d_in" % (snode, sport)
		flow['match']['dst-mac'] = yoda.node_to_vswitch_mac(snode)
		flow['outport'] = 0
		flows.append(flow.copy())

		# install flows
		for flow in flows:
			fields = '\"switch\": \"%s\"' % flow['switch']
			fields += ', \"name\":\"%s\"' % flow['name']

			for crit in flow['match']:
				fields += ', \"%s\":\"%s\"' % (crit, flow['match'][crit])

			fields += ', \"priority\":\"%d\"' % (flow['priority'] if 'priority' in flow else 32768)
			fields += ', \"active\":\"true\"'
			fields += ', \"cookie\":\"0\"'
			fields += ', \"actions\":\"output=%s\"' % flow['outport']

			command = "curl -s -d '{%s}' http://%s/wm/staticflowentrypusher/json" % (fields, ofutil.controllerRestIp)
#			print command
			result = os.popen(command).read()
#			print result

		# ping the destination node
		response = os.system("ssh yoda%d 'ping -W 1 -c 1 yoda%ddata > /dev/null'" % (snode, dnode))

		# and then check the response...
		if response == 0:
			print 'yoda%ddata:%d to yoda%ddata:%d success' % (snode, sport, dnode, dport)
		else:
			print 'yoda%ddata:%d to yoda%ddata:%d FAIL' % (snode, sport, dnode, dport)

		# clear the flows
		ofutil.clearflows_vs()
