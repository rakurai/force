#! /usr/bin/env python

import os
import sys

for snode in range(1, 13):
	for dnode in range(snode+1, 13):
		# ping the destination node
		response = os.system("ssh yoda%d 'ping -W 1 -c 1 yoda%ddata > /dev/null'" % (snode, dnode))

		# and then check the response...
		if response == 0:
			sys.stdout.write('.')
		else:
			print 'yoda%ddata to yoda%ddata FAIL' % (snode, dnode)
