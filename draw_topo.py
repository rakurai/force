#! /usr/bin/env python

import sys
#import os
#sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/src')
import force
import matplotlib.pyplot as plt
import networkx as nx

if __name__ == "__main__":
	usage = "Usage: %s set_file.set" % sys.argv[0]

	if len(sys.argv) != 2:
		sys.exit(usage)

#	layers = force.load_topology(sys.argv[1])
	layers = force.load_layers(force.load_setfile(sys.argv[1]))

	if layers == []:
		sys.exit("Layers file has no readable topology modules.")

	for layer in layers:
		print 'Drawing layer ' + layer['name']
		plt.clf()
		g = layer['graph']
		pos = layer['module'].positions()
		
		path = layer['module'].path(1, 6)
		print path
		color_values = []
		for (src, dst) in g.edges():
			if src in path[:-1] and path[path.index(src)+1] == dst:
				color_values.append(1.0)
				print 'setting color'
			else:
				color_values.append(0.0)

		nx.write_dot(g, 'plot/' + layer['name'] + '.dot')
		nx.draw(
			g,
			pos,
			node_shape='s',
			width=3,
			edge_cmap=plt.get_cmap('jet'),
			edge_color=color_values
		)
		plt.savefig('plot/' + layer['name'] + '.png')
