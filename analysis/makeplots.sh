#!/bin/bash

set=case2
plot.py ${set}/${set}-scores.pkl ${set}/${set}-runtimes.pkl
mv plot.png ${set}/case2-plot.png

banded.py ${set}/${set}-runtimes.pkl ${set}/${set}-scores.pkl
mv plot.png ${set}/case2-banded.png

set=case3
plot2.py ${set}/${set}-scores.pkl ${set}/${set}-meanrates2.pkl
mv plot.png ${set}/case3-plot.png

banded2.py ${set}/${set}-meanrates2.pkl ${set}/${set}-scores.pkl
mv plot.png ${set}/case3-banded.png
