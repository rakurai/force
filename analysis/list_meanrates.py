#! /usr/bin/env python
import sys
import pickle
import forceanalysis as fa

runs = fa.get_logdict(sys.argv[1])
rates = fa.get_meanrates(runs, 'real')

fname = sys.argv[1].split('/')[-1].split('.')[0]
f = open(fname + '-meanrates.pkl', 'w')
pickle.dump(rates, f)
f.close()
