#! /usr/bin/env python
import sys
import pickle
import forceanalysis as fa

runs = fa.get_logdict(sys.argv[1])
scores = fa.get_scores(runs, sys.argv[2])

fname = sys.argv[1].split('/')[-1].split('.')[0]
f = open(fname + '-scores.pkl', 'w')
pickle.dump(scores, f)
f.close()
