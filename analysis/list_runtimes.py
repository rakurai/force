#! /usr/bin/env python
import sys
import pickle
import forceanalysis as fa

runs = fa.get_logdict(sys.argv[1])
times = fa.get_runtimes(runs, 'real')

fname = sys.argv[1].split('/')[-1].split('.')[0]
f = open(fname + '-runtimes.pkl', 'w')
pickle.dump(times, f)
f.close()
