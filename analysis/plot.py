#! /usr/bin/env python
import pickle
import matplotlib.pyplot as plt
from numpy import *
import sys
import forceplot as fp

if len(sys.argv) < 3:
	exit('usage: %s setx.pkl sety.pkl' % sys.argv[0])

f = open(sys.argv[1], 'r')
x = pickle.load(f)
f.close
f = open(sys.argv[2], 'r')
y = pickle.load(f)
f.close

plt.ylim(min(y), max(y))
plt.xlim(min(x), max(x))

#plt.scatter(x, y)
r = corrcoef(x, y)[1,0]

fit = polyfit(x, y, 1)
fit_fn = poly1d(fit)
plt.plot(x, y, 'yo', x, fit_fn(x), '--k')
#plt.ylim(170, 190)
#plt.xlim(0, 1)

print r
xname = sys.argv[1].split('/')[-1].split('.')[0]
yname = sys.argv[2].split('/')[-1].split('.')[0]
fp.xlabel('Link Sharing Score')
fp.ylabel('Completion Time (s)')
#plt.savefig('%s-%s.png' % (xname, yname))
plt.savefig('plot.png', bbox_inches='tight')
