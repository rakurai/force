#! /usr/bin/env python
import sys
import pickle
import forceanalysis as fa

runs = fa.get_logdict(sys.argv[1])
rates = fa.get_meanrates2(runs)

fname = sys.argv[1].split('/')[-1].split('.')[0]
f = open(fname + '-meanrates2.pkl', 'w')
pickle.dump(rates, f)
f.close()
