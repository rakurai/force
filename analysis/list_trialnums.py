#! /usr/bin/env python
import sys
import pickle
import forceanalysis as fa

runs = fa.get_logdict(sys.argv[1])
trials = fa.get_trialnums(runs)

fname = sys.argv[1].split('/')[-1].split('.')[0]
f = open(fname + '-trialnums.pkl', 'w')
pickle.dump(trials, f)
f.close()
