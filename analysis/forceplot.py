#!/usr/bin/env python
import matplotlib.pyplot as plt

def banded_plot(
	yset,
	binvalues,
	bindivisions,
	markers = [],
	colors = [],
	title = ''):

	if markers == []:
		markers = ['o' for i in numbins]
	if colors == []:
		colorlist = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
		colors = [colorlist[i % len(colorlist)] for i in numbins]

	n_items = len(yset)
	n_bins = len(bindivisions)-1
	print n_bins
	xset = range(n_items)

	xbins = [[] for i in range(n_bins)]
	ybins = [[] for i in range(n_bins)]

	for i in range(n_items):
		for j in range(n_bins):
			if binvalues[i] < bindivisions[j]: # get rid of it
				print 'killing %f' % binvalues[i]
				break

			if binvalues[i] <= bindivisions[j+1]:
				xbins[j].append(xset[i])
				ybins[j].append(yset[i])
				break
			

	for i in range(0, n_bins):
		print '%d items in bin %d' % (len(xbins[i]), i)

	plt.ylim(min(yset), max(yset))
	plt.xlim(min(xset), max(xset))

	bintitles = []
	mn = 0
	for bin in range(n_bins):
		plt.scatter(xbins[bin], ybins[bin], color=colors[bin], marker=markers[bin])
		bintitles.append('%.2f to %.2f' % (bindivisions[bin], bindivisions[bin+1]))

	print title
	plt.legend(bintitles, scatterpoints=1, title=title)

	plt.tick_params(\
		axis='x',          # changes apply to the x-axis
		which='both',      # both major and minor ticks are affected
		bottom='off',      # ticks along the bottom edge are off
		top='off',         # ticks along the top edge are off
		labelbottom='off') # labels along the bottom edge are off

def xlabel(label):
	plt.xlabel(label)

def ylabel(label):
	plt.ylabel(label)
