#! /usr/bin/env python
import sys
sys.path.append('/pub/force')
import force
import yoda

def get_runtimes(logdict, type):
	return [run[type] for run in logdict]

def get_meanrates(logdict, type):
	rates = []
	for run in logdict:
		tdata = 0.0

		for reducer in run['transfers']:
			for transfer in run['transfers'][reducer]:
				tdata += transfer['amount']

		trate = 0.0
		for reducer in run['transfers']:
			for transfer in run['transfers'][reducer]:
				rate = transfer['amount'] / transfer[type]
				sig = transfer['amount'] / tdata
				trate += rate * sig

		rates.append(trate*8)
	return rates

def get_meanrates2(logdict):
	outrates = get_node_output_rates(logdict)
	
	rates = []
	for i in range(len(logdict)):
		run = logdict[i]
		outrate = outrates[i]
		total = 0

		for mapper in run['mappers']:
			total += outrate[mapper]

		rates.append(total/len(run['mappers']))
	return rates

# returns a dict = {'yoda1':outrate, 'yoda2':outrate, ...}
def get_node_output_rates(logdict):
	meanlist = []
	for run in logdict:
		bytedict = run['bytes']
		timelist = sorted(bytedict.keys())
		hosts = bytedict[timelist[0]].keys()

		rates = {}
		means = {}

		for i in range(1, len(timelist)):
#			print i
			t1 = timelist[i]
			t0 = timelist[i-1]
			if t1 < t0: t1 += 24*3600
			td = t1 - t0
#			print td

			for host in hosts:
				if host not in rates: rates[host] = []
				rates[host].append((bytedict[timelist[i]][host] - bytedict[timelist[i-1]][host]) / td)
#		print rates
		for host in rates:
			sr = rates[host]
#			sr = sorted(rates[host])
#			if len(sr) >= 4: sr = sr[1:-1]
#			print sr
			ratesum = 0
			for rate in sr: ratesum += rate
			means[host] = ratesum/len(sr)

		meanlist.append(means)
#	print meanlist
	return meanlist

def get_totaltimes(logdict, type):
	times = []
	for run in logdict:
		time = 0.0
		for reducer in run['transfers']:
			for transfer in run['transfers'][reducer]:
				time += transfer[type]
		times.append(time)
	return times

def get_trialnums(logdict):
	return [run['trial'] for run in logdict]

def get_logdict(logfilename):
	f = open(logfilename, 'r')

	runs = []
	defaultconfig = ['yoda%d' % i for i in range(1, 13)]

	for line in f:
		if line[:5] == 'Trial':
			run = {}
			run['transfers'] = {}
			run['mappers'] = []
			run['config'] = defaultconfig
			run['bytes'] = {}
			run['trial'] = int(line.split()[1])
			continue

		if line[:6] == 'Status':
			parts = line.split(' ', 2)

			hour, minute, second = parts[1][1:-1].split(':')
			time = int(hour)*3600 + int(minute)*60 + float(second)

			hs = parts[2][1:-2].split(', ')
			hb = [h[1:] for h in hs]
			hostbytes = {}
			for hostbyte in hb:
				h, b = hostbyte.split("': ")
				hostbytes[h] = int(b)
			run['bytes'][time] = hostbytes
#			print time, hostbytes
			continue

		if line[:3] == "['y": # configuration
			hs = line[1:-2].split(', ')
			hosts = [h[1:-1] for h in hs]
			run['config'] = hosts
			continue

		if line[:4] == 'map:': # transfer
			sections = line.split()
			mapper = sections[1]
			reducer = sections[3]

			if mapper not in run['mappers']:
				run['mappers'].append(mapper) # for meanrates2

			if reducer not in run['transfers']:
				run['transfers'][reducer] = []

			transfer = {}
			transfer['mapper'] = mapper
			transfer['amount'] = int(sections[5])
			run['transfers'][reducer].append(transfer)
			continue

		if line[:2] == "['": # transfer time
			names = ['nothing', 'real', 'user', 'sys']
			for x in range(1, 4):
				time = line.split(', ')[x].split('s\\n')[0].split('t')[1].split('m')
				transfer[names[x]] = int(time[0])*60 + float(time[1])
			continue

		if line[:4] == 'real' or line[:4] == 'user' or line[:3] == 'sys': # total time
			time = line.split('\t')[1][:-2].split('m')
			run[line.split('\t')[0]] = int(time[0])*60 + float(time[1])
			continue

		if line[:9] == 'Completed':
			runs.append(run)
			continue
	f.close()
#	import pprint
#	print pprint.pprint(runs)
	return runs

def get_scores(logdict, topo_filename):
	import lss

	scores = []
	for run in logdict:
		config = run['config']
		nodes = [int(host.split('yoda')[1]) for host in run['config']]
		yoda.node_map = [int(host.split('yoda')[1]) for host in run['config']]

		layers = force.load_topology(topo_filename)

		transfers = []
		for reducer in run['transfers']:
			for transfer in run['transfers'][reducer]:
				transfers.append([transfer['mapper'], reducer])

		score = lss.calculate_lss(layers[-1], transfers)
		scores.append(score)
	return scores
