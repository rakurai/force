input=$1
topo=$2

usage="usage: $0 input.txt topology.set"

if [ -z $input ] || [ -z $topo ]; then
  echo $usage
  exit
fi

# strip the extension
name="${input%.*}"

mkdir -p $name

cp $input $name
cp $topo $name

list_runtimes.py $input
mv $name-runtimes.pkl $name

list_totaltimes.py $input
mv $name-totaltimes.pkl $name

#list_meanrates.py $input
#mv $name-meanrates.pkl $name

list_meanrates2.py $input
mv $name-meanrates2.pkl $name

list_trialnums.py $input
mv $name-trialnums.pkl $name

list_scores.py $input $topo
mv $name-scores.pkl $name


#plot.py $name/scores.pkl $name/times.pkl
#mv scores-times.png $name
