#! /usr/bin/env python
import matplotlib.pyplot as plt
import forceplot as fp
import forceanalysis as fa
import sys
import pickle
import scipy

if __name__ == "__main__":

	if len(sys.argv) < 3:
		exit('usage: %s yfile.pkl binfile.pkl' % sys.argv[0])

	f = open(sys.argv[1], 'r')
	yset = pickle.load(f)
	f.close()
	f = open(sys.argv[2], 'r')
	scores = pickle.load(f)
	f.close()

#	logdict = fa.get_logdict(sys.argv[1])

#	yset = fa.get_totaltimes(logdict, 'real') # flow seconds
#	yset = fa.get_meanrates2(logdict)
#	scores = fa.get_scores(logdict, sys.argv[2])

	m = ['o', '+', '^']
	c = ['r', 'b', 'g']
	bindivisions = [0.4, 0.6, 0.8, 1.0]

	fp.banded_plot(yset, scores, bindivisions, markers = m, colors = c, title = 'Link Sharing Scores')

	fp.ylabel('Completion Time (s)')
#	plt.legend(title='Topology Scores')
	plt.yticks(scipy.arange(15, 44, 5))
	name = sys.argv[1].split('/')[-1].split('.')[0]
	plt.savefig('plot.png', bbox_inches='tight')
